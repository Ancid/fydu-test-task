import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { UserModel } from '../src/users/users.model';
import { UsersModule } from '../src/users/users.module';

describe('Cats', () => {
  const mockUser = new UserModel();
  mockUser.id = 1;
  mockUser.email = 'test@fudy.com';
  mockUser.password = '123456';

  let app: INestApplication;
  const userService = { save: () => mockUser };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [UsersModule],
    })
      .overrideProvider(userService)
      .useValue(userService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`POST /user `, () => {
    return request(app.getHttpServer())
      .post('/user')
      .set('Accept', 'application/json')
      .send(mockUser)
      .expect({
        data: userService.save(),
      })
      .expect(HttpStatus.CREATED);
  });

  it('it should not add a new user if the passed email already exists', () => {
    return request(app.getHttpServer())
      .post('/user')
      .set('Accept', 'application/json')
      .send(mockUser)
      .expect(HttpStatus.BAD_REQUEST);
  });

  afterAll(async () => {
    await app.close();
  });
});
