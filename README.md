# NestJS API service coding exercise
This is a coding exercise done by Anton Sidorov

### Task description
Create a service with endpoints:

- POST /user - creates a new user in database. Payload fields: email and password
- POST /auth/login - checks user credentials. If the credentials are correct, return a JWT token. Payload fields: email and password
- GET /auth/me - returns the current user

### Installing

Run docker and init project once:
```
 make build
```

run tests:
```
 make tests
```

for start and stop the project use:
```
 make up
 make down
```

### Dev notes
- Unit tests can be improved. Coverage is not the best. I just added several tests as examples, not covered all the functionality as most of test will be similar. 
It is a conscious decision due to not much time to finalize the task.
- e2e tests were started to implement but I was not able to run them because of test database configuration. I decided not to continue with tuning as implementing time became longer than expected.  